import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailsMessagePageRoutingModule } from './details-message-routing.module';

import { DetailsMessagePage } from './details-message.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailsMessagePageRoutingModule
  ],
  declarations: [DetailsMessagePage]
})
export class DetailsMessagePageModule {}
