import { Component, OnInit } from '@angular/core';

import { CalendarComponentOptions } from 'ion2-calendar';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {
  showAddEvent: boolean;
  dateRange: { from: string; to: string; };
  type: 'string';
  newEvent = {
    title: '',
    description: '',
    imageURL: '',
    startTime: '',
    endTime: ''
  };
  optionsRange: CalendarComponentOptions = {
    monthFormat: 'MMMM YYYY',
    weekdays: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
    weekStart: 1,
  };
  allEvents = [];

  constructor() { }

  ngOnInit() {
  }

  onChange($event) {
    // console.log($event);
  }

  showHideForm() {
    this.showAddEvent = !this.showAddEvent;
    this.newEvent = {
      title: '',
      description: '',
      imageURL: '',
      startTime: new Date().toISOString(),
      endTime: new Date().toISOString()
    };
  }

  addEvent() {
    this.allEvents.push({
      title: this.newEvent.title,
      startTime:  new Date(this.newEvent.startTime),
      endTime: new Date(this.newEvent.endTime),
      description: this.newEvent.description,
      imageURL: this.newEvent.imageURL
    });
    this.showHideForm();
  }

}
