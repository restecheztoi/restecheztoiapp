import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemandeCoursPage } from './demande-cours.page';

const routes: Routes = [
  {
    path: '',
    component: DemandeCoursPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DemandeCoursPageRoutingModule {}
