import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { PopoverController } from '@ionic/angular';
import { PopoverComponentComponent } from '../popover-component/popover-component.component';
import { Router, NavigationExtras } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment as ENV } from 'src/environments/environment';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {

  currentPopover = null;
  showStudents: boolean;
  public students: any = [];
  public searchTerm = '';
  student: any;
  public grades: any = [];
  grade: any;
  public subjects: any = [];
  subject: any;
  public gradeId: any = [];
  public subjectId: any = [];
  public users: any = [];
  user: any;

  // tslint:disable-next-line:max-line-length
  constructor(public modalController: ModalController, public navCtrl: NavController, public http: HttpClient, private router: Router, public popoverCtrl: PopoverController) {

    // console.log(this.valueStudends);

    this.http.get(`http://${ENV.BACKEND_HOSTNAME}:${ENV.BACKEND_PORT}/subjects/`).subscribe((response) => {
      this.subjects = response;
    });

    this.http.get(`http://${ENV.BACKEND_HOSTNAME}:${ENV.BACKEND_PORT}/grades/`).subscribe((response) => {
      this.grades = response;
    });

    /*this.http.get(`http://${ENV.KEYCLOAK_DOMAIN}:${ENV.KEYCLOAK_PORT}/admin/${ENV.KEYCLOAK_REALM}/users`).subscribe((response) => {
      this.users = response;
      console.log(this.users);
    });*/

    this.showStudents = true;
  }

  onChange() {
    this.http.get(`http://${ENV.BACKEND_HOSTNAME}:${ENV.BACKEND_PORT}/students/?gradeId=${this.gradeId}&subjectId=${this.subjectId}`).subscribe((response) => {
      this.students = response;
    });
  }

  ngOnInit() {}

  presentModalStudent(student) {
  const navigationExtras: NavigationExtras = {
    state: {
      student: student
    }
  };
  this.router.navigate(['details-student'], navigationExtras);
  }

  async presentPopover(myEvent) {
    const popover = await this.popoverCtrl.create({
      component: PopoverComponentComponent,
      event: myEvent,
      showBackdrop: true,
    });
    return await popover.present();
  }

  showHideForm() {
    this.showStudents = !this.showStudents;
  }

}
