import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment as ENV } from 'src/environments/environment';
import { KeycloakService } from '../../services/keycloak.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage implements OnInit {
  categories = ['VALIDATED', 'AWAITING'];
  category: string;
  awaitingCourses;
  validatedCourses;

  constructor(public http: HttpClient, public keycloakService: KeycloakService, private datePipe: DatePipe) {
    this.refreshModel();
  }

  ngOnInit() {
    this.category = this.categories[0];

  }

  refreshModel() {

    this.http.get(`http://${ENV.BACKEND_HOSTNAME}:${ENV.BACKEND_PORT}/courses?state=AWAITING&parentId=${this.keycloakService.getId()}`)
    .subscribe((response) => {
      this.awaitingCourses = response;
      // console.log(this.awaitingCourses);
    });

    this.http.get(`http://${ENV.BACKEND_HOSTNAME}:${ENV.BACKEND_PORT}/courses?state=VALIDATED&parentId=${this.keycloakService.getId()}`)
    .subscribe((response) => {
      this.validatedCourses = response;
      // console.log(this.validatedCourses);
    });

  }

  segmentChanged(ev: any) {
    this.category = ev.detail.value;
    this.refreshModel();
  }

  toDateTimeString(date: string, startTime, endTime) {
    const timeStart = new Date();
    timeStart.setHours(startTime[0]);
    timeStart.setMinutes(startTime[1]);
    const timeEnd = new Date();
    timeEnd.setHours(endTime[0]);
    timeEnd.setMinutes(endTime[1]);

    return new Date(date).toLocaleDateString() + " de " 
    + this.datePipe.transform(timeStart, "HH:mm") + " à " 
    + this.datePipe.transform(timeEnd, "HH:mm");
  }

}
