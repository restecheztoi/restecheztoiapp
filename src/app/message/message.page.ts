import { Component } from '@angular/core';
import { ModalController} from '@ionic/angular';
import { DetailsMessagePage } from '../details-message/details-message.page';


@Component({
  selector: 'app-message',
  templateUrl: 'message.page.html',
  styleUrls: ['message.page.scss']
})
export class MessagePage {
  public messages = [
    {
      id: 1,
      profile_img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSa8HzJvPXzafNqrQwDCCLuqlVhIATF58em81UA--pTKhXrDokJ&usqp=CAU',
      sender: 'Pierre Carré',
      person: 'Pierre Carré',
      last_message: 'Pas de problème.',
      time: '10 min'
    },
    {
      id: 2,
      profile_img: 'https://image.freepik.com/vetores-gratis/personagem-de-avatar-de-mulher-de-negocios_24877-18283.jpg',
      sender: 'Vous',
      person: 'Jack Ben',
      last_message: 'Ok, bye !',
      time: '11:01'
    },
    {
      id: 3,
      profile_img: 'https://image.freepik.com/vetores-gratis/personagem-de-avatar-de-mulher-de-negocios_24877-18283.jpg',
      sender: 'Vous',
      person: 'Marc Smith',
      last_message: 'Max a bien progressé.',
      time: '10:37'
    },
    {
      id: 4,
      profile_img: 'https://image.freepik.com/vetores-gratis/personagem-de-avatar-de-mulher-de-negocios_24877-18282.jpg',
      sender: 'Vanessa LaBriole',
      person: 'Vanessa LaBriole',
      last_message: 'Merci beaucoup, mes enfants sont ravis !',
      time: '8:34'
    },
    {
      id: 5,
      profile_img: 'https://image.freepik.com/vetores-gratis/personagem-de-avatar-de-mulher-de-negocios_24877-18283.jpg',
      sender: 'Vous',
      person: 'Luc Des Près',
      last_message: 'Je vais voir si je suis disponible.',
      time: '2 jours'
    }

  ];

  constructor(public modalController: ModalController) {
  }

  // goNewMessage() {
  //   this.router.navigate(['/new-message']);
  // }

  async goMessageDetail(message) {
      const modal = await this.modalController.create({
        component: DetailsMessagePage,
        componentProps: { message },
        showBackdrop: true,
        backdropDismiss: true
      });
      return await modal.present();
    }




}
