import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { KeycloakService } from '../../services/keycloak.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as ENV } from 'src/environments/environment';

@Component({
  selector: 'app-profile',
  templateUrl: 'profile.page.html',
  styleUrls: ['profile.page.scss']
})
export class ProfilePage implements OnInit {

  newEvent = {
    nom: '',
    age: ''
  };

  options = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  };

  public nomPrenom: any;
  public enfants: any;
  public idParent: any;
  public grades: any;
  public valueGrade: any;

  public profile = {
      id: 1,
      image: 'https://image.freepik.com/vecteurs-libre/personnage-avatar-femme-affaires_24877-18279.jpg',
      description: '4 enfants, éléves de CP, CM1, 5 ème et 3 ème.'
    };

  category: string;
  showAddEvent: boolean;
  constructor(public navCtrl: NavController, private keycloak: KeycloakService, public http: HttpClient) {
    this.keycloak = keycloak;
    this.nomPrenom = this.keycloak.getFullName();
    this.idParent = this.keycloak.getId();
    this.getListChild();
    this.http.get(`http://${ENV.BACKEND_HOSTNAME}:${ENV.BACKEND_PORT}/grades/`).subscribe((response) => {
      this.grades = response;
    });

  }

  getListChild() {
    this.http.get(`http://${ENV.BACKEND_HOSTNAME}:${ENV.BACKEND_PORT}/childs/?parentId=${this.idParent}`).subscribe((response) => {
      this.enfants = response;
    });
  }

  ngOnInit() {
    this.category = 'formations';
  }

  segmentChanged(ev: any) {
    this.category = ev.detail.value;
  }

  showHideForm() {
    this.showAddEvent = !this.showAddEvent;
    this.newEvent = {
      nom: '',
      age: ''
    };
  }

  addChild() {
    const postData = {
      firstname: this.newEvent.nom,
      age: this.newEvent.age,
      parentId: this.idParent,
      grade: {
        id: this.valueGrade.id,
        label: this.valueGrade.label
      }
    };
    // console.log(postData);
    this.http.post(`http://${ENV.BACKEND_HOSTNAME}:${ENV.BACKEND_PORT}/childs`, postData, this.options)
      .subscribe(data => {
        // console.log(data);
        this.getListChild();
        this.showAddEvent = !this.showAddEvent;
       }, error => {
        console.log(error);
      });
  }

  deleteChild(id) {
    // this.http.delete(`http://${ENV.BACKEND_HOSTNAME}:${ENV.BACKEND_PORT}/childs/${id}`, this.options)
    //   .subscribe(data => {
    //     console.log(data);
    //    }, error => {
    //     console.log(error);
    //   });
  }

}
