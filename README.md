# __Welcome to ResteChezToi 📚__
You must make sure that ionic and Cordova are installed globally on your machine.
After that, you can install all the dependencies needed to run our application.

1. First step : 
Check if npm is installed. Otherwise, please install node.js and npm, check the version of npm:  
`npm -v`

2. Then if you do not install ionic and cordova, you can install it with this command:  
`npm install -g cordova ionic`

3. Install all the dependencies listed in package.json:  
`npm install`

4. To launch the application on your browser, run on the command line in a terminal:  
`ionic serve`

----------------------

If you want to run the application on your mobile or emulator:

*Android :*
- Add an Android to the project --> `ionic cordova platform add android`
- Run the application on your Android device -->  `ionic cordova run android`

*iOS :*
- Add an iOS to the project --> `ionic cordova platform add ios`
- Run the app on your iOS device --> `ionic cordova run ios`


