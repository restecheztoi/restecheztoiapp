import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailsMessagePage } from './details-message.page';

const routes: Routes = [
  {
    path: '',
    component: DetailsMessagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailsMessagePageRoutingModule {}
