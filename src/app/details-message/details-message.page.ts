import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController} from '@ionic/angular';

@Component({
  selector: 'app-details-message',
  templateUrl: './details-message.page.html',
  styleUrls: ['./details-message.page.scss'],
})
export class DetailsMessagePage implements OnInit {

  constructor(public navParams: NavParams, public modalController: ModalController) {
  }


  ngOnInit() {
  }

}
