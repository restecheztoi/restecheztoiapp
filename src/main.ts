import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { KeycloakService } from './services/keycloak.service';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
//require('dotenv').config()

if (environment.production) {
  enableProdMode();
}

// platformBrowserDynamic().bootstrapModule(AppModule)
//   .catch(err => console.log(err));

KeycloakService.init()
.then(() => {
  const platform = platformBrowserDynamic();
  // Mnaually intiliase angular
  platform.bootstrapModule(AppModule);
})
.catch((err) => console.error('Error initalizing Keycloak', err));
