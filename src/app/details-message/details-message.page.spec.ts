import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailsMessagePage } from './details-message.page';

describe('DetailsMessagePage', () => {
  let component: DetailsMessagePage;
  let fixture: ComponentFixture<DetailsMessagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsMessagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailsMessagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
