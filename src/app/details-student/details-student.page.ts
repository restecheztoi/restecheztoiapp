import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { PopoverController } from '@ionic/angular';
import { PopoverComponentComponent } from '../popover-component/popover-component.component';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment as ENV } from 'src/environments/environment';

@Component({
  selector: 'app-details-student',
  templateUrl: './details-student.page.html',
  styleUrls: ['./details-student.page.scss'],
})
export class DetailsStudentPage implements OnInit {
  category: string;
  showDocument: boolean;
  student: any;
  public subjectId: any = [];
  public subjects: any = [];
  public grades: any = [];
  public studentName: any;
  grade: any;
  // tslint:disable-next-line:max-line-length
  constructor(public http: HttpClient, public modalController: ModalController, public navCtrl: NavController, public popoverCtrl: PopoverController, private route: ActivatedRoute, private router: Router) {
    this.showDocument = true;
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.student = this.router.getCurrentNavigation().extras.state.student;
      }

      this.http.get(`http://${ENV.BACKEND_HOSTNAME}:${ENV.BACKEND_PORT}/subjects/?studentId=${this.student.id}`).subscribe((response) => {
        this.subjects = response;
      });

      // tslint:disable-next-line:max-line-length
      this.http.get(`http://${ENV.BACKEND_HOSTNAME}:${ENV.BACKEND_PORT}/grades/?studentId=${this.student.id}&subjectId=${this.subjects.id}`).subscribe((response) => {
        this.grades = response;
        // console.log(this.grades);
      });
    });
  }

  ngOnInit() {
    this.category = 'apropos';
  }

  segmentChanged(ev: any) {
    this.category = ev.detail.value;
  }

  async presentPopover(myEvent) {
    // this.detailList = true;
    const popover = await this.popoverCtrl.create({
      component: PopoverComponentComponent,
      // componentProps: { show: this.detailList },
      event: myEvent,
      showBackdrop: true,
    });
    return await popover.present();
  }

   presentModalDocument() {
    this.showDocument = !this.showDocument;
  }


  gotoDemandCourse(prenom, nom, id) {
    const navigationExtras: NavigationExtras = {
      state: {
        studentPrenom: prenom,
        studentNom: nom,
        studentId: id
      }
    };
    this.router.navigate(['demande-cours'], navigationExtras);
  }

}

