import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailsStudentPage } from './details-student.page';

describe('DetailsStudentPage', () => {
  let component: DetailsStudentPage;
  let fixture: ComponentFixture<DetailsStudentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsStudentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailsStudentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
