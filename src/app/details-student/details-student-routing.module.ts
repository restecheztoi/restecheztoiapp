import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailsStudentPage } from './details-student.page';

const routes: Routes = [
  {
    path: '',
    component: DetailsStudentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailsStudentPageRoutingModule {}
