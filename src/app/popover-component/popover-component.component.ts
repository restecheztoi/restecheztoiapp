import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
// import { AproposPage } from '../apropos/apropos.page';
import { Router } from '@angular/router';


@Component({
  selector: 'app-popover-component',
  templateUrl: './popover-component.component.html',
  styleUrls: ['./popover-component.component.scss'],
})
export class PopoverComponentComponent implements OnInit {

  // showDetails: boolean;

  constructor(public navCtrl: NavController, public modalController: ModalController, private routes: Router) {
    // this.showDetails = show;
  }

  ngOnInit() {}

  // async presentModalApropos() {
  //   const modal = await this.modalController.create({
  //     component: AproposPage,
  //     componentProps: { },
  //     showBackdrop: true,
  //     backdropDismiss: true
  //   });
  //   return await modal.present();
  // }

}
