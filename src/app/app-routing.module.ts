import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  // { path: '', redirectTo: '/login', pathMatch: 'full'},
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule' },
  { path: 'message', loadChildren: './message/message.module#MessagePageModule' },
  { path: 'search', loadChildren: './search/search.module#SearchPageModule' },
  { path: 'list', loadChildren: './list/list.module#ListPageModule' },
  {
    path: 'details-message',
    loadChildren: () => import('./details-message/details-message.module').then( m => m.DetailsMessagePageModule)
  },
  {
    path: 'details-student',
    loadChildren: () => import('./details-student/details-student.module').then( m => m.DetailsStudentPageModule)
  },
  {
    path: 'demande-cours',
    loadChildren: () => import('./demande-cours/demande-cours.module').then( m => m.DemandeCoursPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
