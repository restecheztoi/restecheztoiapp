import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DemandeCoursPageRoutingModule } from './demande-cours-routing.module';

import { DemandeCoursPage } from './demande-cours.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DemandeCoursPageRoutingModule
  ],
  declarations: [DemandeCoursPage]
})
export class DemandeCoursPageModule {}
