import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { KeycloakService } from '../services/keycloak.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
  providers: [KeycloakService]
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public nomPrenom: any;
  public appPages = [
    {
      title: 'Donner son avis',
      url: '',
      icon: 'thumbs-up'
    },
    {
      title: 'Politique de confidentialité',
      url: '',
      icon: 'lock-closed'
    },
    {
      title: 'Conditions de services',
      url: '',
      icon: 'reader'
    },
    {
      title: 'A propos',
      url: '',
      icon: 'information-circle'
    },
    {
      title: 'Aide',
      url: '',
      icon: 'help-circle'
    },
    {
      title: 'Réglages',
      url: '',
      icon: 'settings'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private keycloak: KeycloakService
  ) {
    this.keycloak = keycloak;
    this.initializeApp();
    window.localStorage.setItem('kcToken', this.keycloak.getToken());
    this.nomPrenom = this.keycloak.getFullName();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
  }


  logout(): void {
    this.keycloak.logout();
  }
}

