import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailsStudentPageRoutingModule } from './details-student-routing.module';

import { DetailsStudentPage } from './details-student.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailsStudentPageRoutingModule
  ],
  declarations: [DetailsStudentPage]
})
export class DetailsStudentPageModule {}
