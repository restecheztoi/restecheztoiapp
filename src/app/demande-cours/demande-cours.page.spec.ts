import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DemandeCoursPage } from './demande-cours.page';

describe('DemandeCoursPage', () => {
  let component: DemandeCoursPage;
  let fixture: ComponentFixture<DemandeCoursPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemandeCoursPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DemandeCoursPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
