import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as ENV } from 'src/environments/environment';
import { KeycloakService } from '../../services/keycloak.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-demande-cours',
  templateUrl: './demande-cours.page.html',
  styleUrls: ['./demande-cours.page.scss'],
})
export class DemandeCoursPage implements OnInit {

  public studentName: any;
  public subjects: any;

  dateRange: { from: string; to: string; };
  type: 'string';
  newEvent = {
    nom: '',
    matière: '',
    day: new Date().toISOString(),
    startTime: new Date().toISOString(),
    endTime: new Date().toISOString()
  };
  public idStudent: any;
  options = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  };

  public valueSubject: any;
  public enfants: any;
  public valueEnfant: any;
  public awaitingCourses: any;
  public validatedCourses: any;


  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private route: ActivatedRoute, public http: HttpClient, private keycloak: KeycloakService, private datePipe: DatePipe) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        // tslint:disable-next-line:max-line-length
        this.studentName = this.router.getCurrentNavigation().extras.state.studentPrenom + ' ' + this.router.getCurrentNavigation().extras.state.studentNom;
        this.idStudent = this.router.getCurrentNavigation().extras.state.studentId;
      }
    });
    // tslint:disable-next-line:max-line-length
    this.http.get(`http://${ENV.BACKEND_HOSTNAME}:${ENV.BACKEND_PORT}/childs/?parentId=${this.keycloak.getId()}`).subscribe((response) => {
      this.enfants = response;
    });
    this.http.get(`http://${ENV.BACKEND_HOSTNAME}:${ENV.BACKEND_PORT}/subjects/`).subscribe((response) => {
      this.subjects = response;
    });
  }

  ngOnInit() {
  }


  refreshModel() {
    this.http.get(`http://${ENV.BACKEND_HOSTNAME}:${ENV.BACKEND_PORT}/courses?state=AWAITING&parentId=${this.keycloak.getId()}`)
    .subscribe((response) => {
      this.awaitingCourses = response;
      // console.log(this.awaitingCourses);
    });

    this.http.get(`http://${ENV.BACKEND_HOSTNAME}:${ENV.BACKEND_PORT}/courses?state=VALIDATED&parentId=${this.keycloak.getId()}`)
    .subscribe((response) => {
      this.validatedCourses = response;
      // console.log(this.validatedCourses);
    });

  }

  addDemand() {

    const postData = {
      child: {
        id: this.valueEnfant.id,
        firstname: '',
        age: 0,
        parentId: '',
        grade: {
          id: '',
          label: ''
        }
      },
      date: this.datePipe.transform(this.newEvent.day, 'yyyy-MM-dd'),
      timeStart: this.datePipe.transform(this.newEvent.startTime, 'HH:mm:ss'),
      timeEnd: this.datePipe.transform(this.newEvent.endTime, 'HH:mm:ss'),
      student: {
        id: this.idStudent,
        first_name: '',
        student_grade: '',
        last_name: '',
        formation: '',
        phoneNumber: '',
        school: ''
      },
      state: 'AWAITING',
      subject: {
        id: this.valueSubject.id,
        label: this.valueSubject.label
      }
    };

    // console.log(postData);

    this.http.post(`http://${ENV.BACKEND_HOSTNAME}:${ENV.BACKEND_PORT}/courses`, postData, this.options)
      .subscribe(data => {
        // console.log(data);
        this.refreshModel();
        this.router.navigate(['tabs/home']);
       }, error => {
        console.log(error);
      });


  }

}
