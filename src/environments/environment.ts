// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  HOSTNAME: 'localhost',
  PORT: '8100',

  BACKEND_HOSTNAME: 'localhost',
  BACKEND_PORT: '8080',

  KEYCLOAK_PROTOCOL: 'http',
  KEYCLOAK_DOMAIN: '163.172.128.71',
  KEYCLOAK_PORT: '8080',
  KEYCLOAK_REALM: 'restecheztoi',
  KEYCLOAK_CLIENT_ID: 'restecheztoi',
  KEYCLOAK_CLIENT_SECRET: 'd08faa65 - 9de9- 4f2c-a765 - 6d239795c292',

  REQUESTED_RANGE: 10,
  MAX_RANGE: 100,
  CONTENT_SIZE: 50000
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
